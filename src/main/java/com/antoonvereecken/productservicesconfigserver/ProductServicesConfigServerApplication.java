package com.antoonvereecken.productservicesconfigserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class ProductServicesConfigServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductServicesConfigServerApplication.class, args);
	}

}
